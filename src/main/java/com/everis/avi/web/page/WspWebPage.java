package com.everis.avi.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class WspWebPage {

    public void TiempoEspera() throws InterruptedException{
        Thread.sleep(10000);

    }
    public void AbrirPrimerChat(){
        WebElement btnChatTry = getDriver().findElement(By.xpath("//*[@id=\"pane-side\"]/div[1]/div/div/div[1]/div/div/div[2]"));
        btnChatTry.click();
    }

    public void IngresarChatMensaje(){
        WebElement btnEnviarMensaje = getDriver().findElement(By.xpath("//*[@id=\"action-button\"]"));
        WebElement btnIngresarWspWeb = getDriver().findElement(By.xpath("//*[@id=\"fallback_block\"]/div/div/a"));


        btnEnviarMensaje.click();

        btnIngresarWspWeb.click();


    }
    public void EscribirChat(String sTextoMensaje){
        WebElement txtMensaje = getDriver().findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div[2]/div/div[2]"));
        WebElement btnSend = getDriver().findElement(By.xpath("   //*[@id=\"main\"]/footer/div[1]/div[3]"));

        txtMensaje.sendKeys(sTextoMensaje);
        System.out.println(sTextoMensaje);
        if(sTextoMensaje.equals(""))
        {

        }
        else
        btnSend.click();
    }

    public String ObtenerRespuestaTry(int iIndiceDialogo) throws InterruptedException {
        String sDialigoTry = "";
        String sDialigoTry2 = "";
        Thread.sleep(1000);
        try {


        sDialigoTry  = getDriver().findElements(By.xpath("/html/body/div[1]/div/div/div[4]/div/div[3]/div/div/div[3]")).get(iIndiceDialogo).getText();
      //  sDialigoTry2 = getDriver().findElements(By.xpath("/html/body/div[1]/div/div/div[4]/div/div[3]/div/div/div[3]/div[26]/div/div/div/div[1]/div/span[1]/span"))
        }
        catch(Exception e)
        {

        }

        return sDialigoTry;
    }
}
