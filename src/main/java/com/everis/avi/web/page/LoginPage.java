package com.everis.avi.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class LoginPage {

    public void AccedeLogin(String sUser, String sPass) throws InterruptedException {
        WebElement txtUser = getDriver().findElement(By.id("username"));
        WebElement btnContinuar = getDriver().findElement(By.id("continue-button"));
        Thread.sleep(2000);
        txtUser.sendKeys(sUser.trim());
        btnContinuar.click();
        Thread.sleep(2000);
        WebElement txtPass = getDriver().findElement(By.id("password"));
        txtPass.sendKeys(sPass.trim());
        WebElement btnIngresar = getDriver().findElement(By.id("signinbutton"));
        btnIngresar.click();
    }

    public void QuitarAlertas() throws InterruptedException {
        getDriver().switchTo().frame(getDriver().findElement(By.xpath("//iframe[@tabindex='1'][@scrolling='no']")));
        WebElement btnAlerta = getDriver().findElement(By.xpath("//a[@class='call']"));
        btnAlerta.click();
        Thread.sleep(4000);

        getDriver().switchTo().frame(getDriver().findElement(By.xpath("//iframe[@tabindex='1'][@scrolling='no']")));
    }

    public void SeleccionarTarjeta() {
        WebElement trjAMbienteWs = getDriver().findElement(By.xpath("//div[@title='AUTO-AI-Santiago']"));
        trjAMbienteWs.click();
    }

    public void SeleccionarTarjetaWs() {
        WebElement trjAMbienteWs = getDriver().findElement(By.xpath("//h5[@title='AUTO-WS-Santiago']"));
        trjAMbienteWs.click();
    }

    public void ConfigurarDIalogoWs() {
        WebElement lnkDialogo = getDriver().findElement(By.id("Dialog-navigation-link"));
        lnkDialogo.click();

        WebElement trjConfiguracion = getDriver().findElement(By.id("node_5_1566342788101"));
        trjConfiguracion.click();
        //  trjConfiguracion.click();
    }

    public void AbrirchatTry() {
        WebElement btnChatTry = getDriver().findElement(By.id("workspace-actions__tryItOpen"));
        btnChatTry.click();
    }

    public void EscribirChat(String sTextoMensaje) {
        WebElement txtMensaje = getDriver().findElement(By.id("-chatPanel__submitInput"));

        txtMensaje.sendKeys(sTextoMensaje);
        txtMensaje.submit();
    }

    public String ObtenerRespuestaTry(int iIndiceDialogo) throws InterruptedException {
        String sDialigoTry = "";
        Thread.sleep(1000);
        try {


            sDialigoTry = getDriver().findElements(By.xpath("//fieldset[@class='watson-utt']")).get(iIndiceDialogo).getText();
        } catch (Exception e) {
            System.out.println("Error metodo ObtenerRespuestaTry" + e.getMessage());
        }

        return sDialigoTry;
    }
}
