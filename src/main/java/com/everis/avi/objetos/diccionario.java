package com.everis.avi.objetos;

public class diccionario {
    int iCodigoDiccionarioHistorico;
    String cPosicionFlujo;
    String cIndicadorFormulario;
    String vAsesor;
    String vBoton;
    String vBot;
    String cCodigoRespuesta;
    String vRespuestaObtenida;
    String cCodigoObtenido;
    String cResultado;
    int iCodigoDiccionarioTipo;
    int iCodigoBot;
    int iCodigoUsuario;

    public int getiCodigoDiccionarioHistorico() {
        return this.iCodigoDiccionarioHistorico;
    }

    public void setiCodigoDiccionarioHistorico(int iCodigoDiccionarioHistorico) {
        this.iCodigoDiccionarioHistorico = iCodigoDiccionarioHistorico;
    }

    public String getcPosicionFlujo() {
        return this.cPosicionFlujo;
    }

    public void setcPosicionFlujo(String cPosicionFlujo) {
        this.cPosicionFlujo = cPosicionFlujo;
    }

    public String getcIndicadorFormulario() {
        return this.cIndicadorFormulario;
    }

    public void setcIndicadorFormulario(String cIndicadorFormulario) {
        this.cIndicadorFormulario = cIndicadorFormulario;
    }

    public String getvAsesor() {
        return this.vAsesor;
    }

    public void setvAsesor(String vAsesor) {
        this.vAsesor = vAsesor;
    }

    public String getvBoton() {
        return this.vBoton;
    }

    public void setvBoton(String vBoton) {
        this.vBoton = vBoton;
    }

    public String getvBot() {
        return this.vBot;
    }

    public void setvBot(String vBot) {
        this.vBot = vBot;
    }

    public String getcCodigoRespuesta() {
        return this.cCodigoRespuesta;
    }

    public void setcCodigoRespuesta(String cCodigoRespuesta) {
        this.cCodigoRespuesta = cCodigoRespuesta;
    }

    public String getvRespuestaObtenida() {
        return this.vRespuestaObtenida;
    }

    public void setvRespuestaObtenida(String vRespuestaObtenida) {
        this.vRespuestaObtenida = vRespuestaObtenida;
    }

    public String getcCodigoObtenido() {
        return this.cCodigoObtenido;
    }

    public void setcCodigoObtenido(String cCodigoObtenido) {
        this.cCodigoObtenido = cCodigoObtenido;
    }

    public String getcResultado() {
        return this.cResultado;
    }

    public void setcResultado(String cResultado) {
        this.cResultado = cResultado;
    }

    public int getiCodigoDiccionarioTipo() {
        return iCodigoDiccionarioTipo;
    }

    public void setiCodigoDiccionarioTipo(int iCodigoDiccionarioTipo) {
        this.iCodigoDiccionarioTipo = iCodigoDiccionarioTipo;
    }

    public int getiCodigoBot() {
        return iCodigoBot;
    }

    public void setiCodigoBot(int iCodigoBot) {
        this.iCodigoBot = iCodigoBot;
    }

    public int getiCodigoUsuario() {
        return iCodigoUsuario;
    }

    public void setiCodigoUsuario(int iCodigoUsuario) {
        this.iCodigoUsuario = iCodigoUsuario;
    }
}
