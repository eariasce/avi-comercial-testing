package com.everis.avi.rest.task;

import com.everis.avi.util.Util;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class DialogoAvi_Beta implements Task {

    private static String URL = "https://apis.dev.interbank.pe/eva/conversations/";
    private static final String TEMPLATE_DIALOGO = "/templates/dialogo.json";

    private String TextoDilogoAvi;
    private String keyDialogo;

    public DialogoAvi_Beta(String TextoDilogoAvi, String keyDialogo) {
        this.TextoDilogoAvi = TextoDilogoAvi;
        this.keyDialogo = keyDialogo;
    }

    public static Performable withDialogo(String TextoDilogoAvi, String keyDialogo) {
        return Tasks.instrumented(DialogoAvi_Beta.class, TextoDilogoAvi, keyDialogo);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(keyDialogo).with(request -> request
                .contentType(ContentType.JSON)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("API-KEY", "20dcb8cb-4603-4efa-a78a-8bb4f83ce46a")
                .header("PROJECT", "EXT_COMERCIAL")
                .header("CHANNEL", "WAPP_COM")
                .header("OS", "android")
                //DEV
                //.header("USER-REF", "51995757703")
                //UAT
                .header("USER-REF", "51995757703")
                .header("LOCALE", "es-ES")
                .header("OS-VERSION", "10")
                .header("BROWSER", "Chrome")
                .header("BROWSER-VERSION", "10")
                .header("BUSINESS-KEY", "123")
                //KEY DEV
                //.header("Ocp-Apim-Subscription-Key", "3b8700ab20814ee58e07ffc89e16c86d")
                //KEY UAT
                .header("Ocp-Apim-Subscription-Key", "c9664a560c184e8cb857e5d2a7efabc4")
                //NUEVO KEY UAT2
                //.header("Ocp-Apim-Subscription-Key", "")
                //NUEVO KEY STG
                //.header("Ocp-Apim-Subscription-Key", "")
                .body(Util.getTemplate(TEMPLATE_DIALOGO)
                        .replace("{text}", TextoDilogoAvi))));
    }
}